package lib

import (
	"log"
	"os"
	"strconv"

	scraping "example.com/post-qiita-daily-trends/lib/scraping"
	timezone "example.com/post-qiita-daily-trends/lib/timezone"
	slack "github.com/slack-go/slack"
)

// Post : Slackへ通知する
func Post(dailyTrends scraping.DailyTrends) {
	api := slack.New(os.Getenv("SLACK_TOKEN"))

	_, _, err := api.PostMessage(
		os.Getenv("SLACK_CHANNEL"),
		createBlocks(dailyTrends),
	)

	if err != nil {
		log.Println(err)
		return
	}
}

// メッセージを生成
func createMessage(ranking int, link string, like float64) string {
	return "" + strconv.Itoa(ranking) + "位\n" + link + "\nLGTM " + strconv.FormatFloat(like, 'f', 0, 64) + " :+1:"
}

// Slackへ投稿するBlock部を生成 (Qiita日別ランキングの上位20件)
func createBlocks(dailyTrends scraping.DailyTrends) slack.MsgOption {
	var section [30]slack.Block

	// Divセクション
	divSection := slack.NewDividerBlock()
	// ヘッダーセクション
	headerText := slack.NewTextBlockObject("mrkdwn", "*"+timezone.GetCurrentDate()+" のランキング*", false, false)
	headerSection := slack.NewSectionBlock(headerText, nil, nil)
	// メインコンテンツセクション (Qiitaの日別トレンド部分)
	for index, dailyTrend := range dailyTrends {
		text := slack.NewTextBlockObject("mrkdwn", createMessage(dailyTrend.Rank, dailyTrend.Link, dailyTrend.Like), false, false)
		accessory := slack.NewImageBlockElement(dailyTrend.Image, "user thumbnail")
		section[index] = slack.NewSectionBlock(text, nil, slack.NewAccessory(accessory))
	}

	return slack.MsgOptionBlocks(
		headerSection, divSection,
		// 1位～5位
		section[0], divSection, section[1], divSection, section[2], divSection, section[3], divSection, section[4], divSection,
		// 5位～10位
		section[5], divSection, section[6], divSection, section[7], divSection, section[8], divSection, section[9], divSection,
		// 11位～15位
		section[10], divSection, section[11], divSection, section[12], divSection, section[13], divSection, section[14], divSection,
		// 16位～20位
		section[15], divSection, section[16], divSection, section[17], divSection, section[18], divSection, section[19], divSection,
	)
}
