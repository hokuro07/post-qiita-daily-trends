package lib

import (
	"time"
)

// SetJpTimeZone : Asia/Tokyoをタイムゾーンを指定
func SetJpTimeZone() {
	time.Local = time.FixedZone("Asia/Tokyo", 9*60*60)
	time.LoadLocation("Asia/Tokyo")
}

// GetCurrentDate : 現在年月日を取得
func GetCurrentDate() string {
	cTime := time.Now()
	layout := "2006/01/02"
	return cTime.Format(layout)
}
