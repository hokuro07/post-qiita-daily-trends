package lib

import (
	"encoding/json"
	"log"

	goquery "github.com/PuerkitoBio/goquery"
)

// 定数
const (
	QIITAURL             = "https://qiita.com/"
	msgURLNotFound       = "[ERROR] : URL Not Found !"
	msgSelectionNotFound = "[ERROR] : Selection Not Found !"
)

// DailyTrend : Qiitaから取得した日別トレンド情報
type DailyTrend struct {
	Rank  int
	Link  string
	Image string
	Like  float64
}

// DailyTrends : 日別トレンド情報群
type DailyTrends []DailyTrend

// CreateLink : Qiitaの各記事のリンクを生成する
func CreateLink(title string, name string, uuid string) string {
	return "<" + QIITAURL + name + "/items/" + uuid + "|" + title + ">"
}

// FetchDailyTrend : Qiitaからスクレイピングして日別トレンドを取得する
func FetchDailyTrend() DailyTrends {
	// 初期値
	var dailyTrends DailyTrends

	// 対象URLからドキュメント取得
	doc, err := goquery.NewDocument(QIITAURL)
	if err != nil {
		log.Println(msgURLNotFound)
	} else {
		// ドキュメントからセレクションを取得
		doc.Find(".p-home_main .js-react-on-rails-component").Each(func(index int, selection *goquery.Selection) {
			// <script>data</script> この形式で情報を保持するように形が変わった模様
			text := selection.Text()

			if len(text) == 0 {
				log.Println(msgSelectionNotFound)
			} else {
				var props map[string]interface{}

				// jsonをmapへデコード
				if err := json.Unmarshal([]byte(text), &props); err != nil {
					// nilでない場合はエラー
					log.Println(err)
				} else {
					trend := props["trend"].(map[string]interface{})
					edges := trend["edges"].([]interface{})

					// 1位からインクリメント
					ranking := 1
					for _, edge := range edges {
						if edgeVal, ok := edge.(map[string]interface{}); ok {
							if node, ok := edgeVal["node"].(map[string]interface{}); ok {
								if author, ok := node["author"].(map[string]interface{}); ok {
									dailyTrends = append(dailyTrends, DailyTrend{
										Rank:  ranking,
										Link:  CreateLink(node["title"].(string), author["urlName"].(string), node["uuid"].(string)),
										Image: author["profileImageUrl"].(string),
										Like:  node["likesCount"].(float64),
									})
								}
							}
						}
						ranking++
					}
				}
			}
		}) // end Each
	}

	return dailyTrends
}
