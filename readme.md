# post-qiita-daily-trends
Qiitaデイリートレンドをスクレイピングで取得し、Slackへ通知するツール 

# デモ
![デモ画像](https://gitlab.com/hokuro07/post-qiita-daily-trends/uploads/76f2b849ef11b6aa605e2f06dbfbdcf8/sample.png "デモ画像")

# Go言語バージョン
go version go1.13.x

# 実行環境（想定）
下記3つを利用した実行環境を想定して作りました。

* Google Cloud Functions
* Google Cloud Scheduler
* Google Cloud Pub/Sub

Cloud Functionsの環境変数に以下を設定し、QiitaPubSub関数をキックするようにしてください。  

```
SLACK_TOKEN : Slackのトークン
SLACK_CHANNEL : SlackのチャネルID
```

# 注意
Qiita公式サイトからスクレイピングしてデータの取得を行います。  
運営を妨げることがないよう、ご利用ください。

また本ツール使用上起きた問題についての責任は負いかねます。  
自己責任で利用してください。

# ライセンス 
"post-qiita-daily-trends" は[MIT license](https://en.wikipedia.org/wiki/MIT_License)で配布しています。
