package p

import (
	"context"

	scraping "example.com/post-qiita-daily-trends/lib/scraping"
	slack "example.com/post-qiita-daily-trends/lib/slack"
	timezone "example.com/post-qiita-daily-trends/lib/timezone"
)

// PubSubMessage is the payload of a Pub/Sub event.
type PubSubMessage struct {
	Data []byte `json:"data"`
}

// QiitaPubSub consumes a Pub/Sub message.
func QiitaPubSub(ctx context.Context, m PubSubMessage) error {
	// 日別トレンドを取得
	dailyTrends := scraping.FetchDailyTrend()
	// 取得した日別トレンドをSlackへ投稿
	slack.Post(dailyTrends)

	return nil
}

func init() {
	timezone.SetJpTimeZone()
}
