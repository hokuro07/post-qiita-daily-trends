module example.com/post-qiita-daily-trends

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/slack-go/slack v0.6.5
)
